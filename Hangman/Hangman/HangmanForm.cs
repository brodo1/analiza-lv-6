﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hangman
{
    
    public partial class Form1 : Form
    {
        string secretWord;
        int livesleft=5;
        bool c1guessed, c2guessed, c3guessed, c4guessed, c5guessed, c6guessed,Won;

        

        char c1, c2, c3, c4, c5, c6;
        private void BTN_Guess_Click(object sender, EventArgs e)
        {
            if (textBoxGuess.Text.Length > 1)
            {
                MessageBox.Show(" cant put more than 1 character","Hangman");
            }
            if (secretWord.Contains(textBoxGuess.Text.ToLower()))
            {
                if(textBoxGuess.Text == c1.ToString())
                {
                    LB_Char1.Text = c1.ToString();
                    c1guessed = true;
                }
                if (textBoxGuess.Text == c2.ToString())
                {
                    LB_Char2.Text = c2.ToString();
                    c2guessed = true;
                }
                if (textBoxGuess.Text == c3.ToString())
                {
                    LB_Char3.Text = c3.ToString();
                    c3guessed = true;
                }
                if (textBoxGuess.Text == c4.ToString())
                {
                    LB_Char4.Text = c4.ToString();
                    c4guessed = true;
                }
                if (textBoxGuess.Text == c5.ToString())
                {
                    LB_Char5.Text = c5.ToString();
                    c5guessed = true;
                }
                if (textBoxGuess.Text == c6.ToString())
                {
                    LB_Char6.Text = c6.ToString();
                    c6guessed = true;
                }
                if (c1guessed && c2guessed && c3guessed && c4guessed && c5guessed && c6guessed)
                {
                    Won = true;
                }
                if (Won)
                {
                    MessageBox.Show("you have won!! " + livesleft.ToString() + " lives left", "Hangman");
                }
            }
            else
            {
                livesleft--;
                LB_LifeValue.Text = livesleft.ToString();
                TB_missedwords.Text += textBoxGuess.Text;
                if (LB_LifeValue.Text=="0")
                {
                    MessageBox.Show("you have lost! Word is:"+ secretWord);
                    Won = false;
                    BTN_Guess.Visible = false;
                }
            }
            
            
        }

        public Form1()
        {
            InitializeComponent();
        }


        private void BTN_Play_Click(object sender, EventArgs e)
        {
            Resetter();
            secretWord = GetWord.WordGetter();
           
            c1 = secretWord[0]; c2 = secretWord[1]; c3 = secretWord[2]; c4 = secretWord[3]; c5 = secretWord[4]; c6 = secretWord[5];
            LB_LifeValue.Text = livesleft.ToString();
            MessageBox.Show(secretWord);
            MessageBox.Show("new word has been generated","Hangman");
        }
        private void Resetter()
        {
            Won = false; c1guessed = false; c2guessed = false; c3guessed = false; c4guessed = false; c5guessed = false; c6guessed = false; BTN_Guess.Visible = true;

        }
    }
}
