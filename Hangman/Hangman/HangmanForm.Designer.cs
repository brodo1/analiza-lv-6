﻿namespace Hangman
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LB_Char1 = new System.Windows.Forms.Label();
            this.LB_Char2 = new System.Windows.Forms.Label();
            this.LB_Char3 = new System.Windows.Forms.Label();
            this.LB_Char4 = new System.Windows.Forms.Label();
            this.LB_Char5 = new System.Windows.Forms.Label();
            this.LB_Char6 = new System.Windows.Forms.Label();
            this.lbl7 = new System.Windows.Forms.Label();
            this.BTN_Guess = new System.Windows.Forms.Button();
            this.BTN_Play = new System.Windows.Forms.Button();
            this.textBoxGuess = new System.Windows.Forms.TextBox();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.LB_LifeValue = new System.Windows.Forms.Label();
            this.TB_missedwords = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // LB_Char1
            // 
            this.LB_Char1.AutoSize = true;
            this.LB_Char1.Location = new System.Drawing.Point(125, 170);
            this.LB_Char1.Name = "LB_Char1";
            this.LB_Char1.Size = new System.Drawing.Size(13, 17);
            this.LB_Char1.TabIndex = 0;
            this.LB_Char1.Text = "*";
            // 
            // LB_Char2
            // 
            this.LB_Char2.AutoSize = true;
            this.LB_Char2.Location = new System.Drawing.Point(218, 170);
            this.LB_Char2.Name = "LB_Char2";
            this.LB_Char2.Size = new System.Drawing.Size(13, 17);
            this.LB_Char2.TabIndex = 1;
            this.LB_Char2.Text = "*";
            // 
            // LB_Char3
            // 
            this.LB_Char3.AutoSize = true;
            this.LB_Char3.Location = new System.Drawing.Point(301, 170);
            this.LB_Char3.Name = "LB_Char3";
            this.LB_Char3.Size = new System.Drawing.Size(13, 17);
            this.LB_Char3.TabIndex = 2;
            this.LB_Char3.Text = "*";
            // 
            // LB_Char4
            // 
            this.LB_Char4.AutoSize = true;
            this.LB_Char4.Location = new System.Drawing.Point(391, 170);
            this.LB_Char4.Name = "LB_Char4";
            this.LB_Char4.Size = new System.Drawing.Size(13, 17);
            this.LB_Char4.TabIndex = 3;
            this.LB_Char4.Text = "*";
            // 
            // LB_Char5
            // 
            this.LB_Char5.AutoSize = true;
            this.LB_Char5.Location = new System.Drawing.Point(474, 170);
            this.LB_Char5.Name = "LB_Char5";
            this.LB_Char5.Size = new System.Drawing.Size(13, 17);
            this.LB_Char5.TabIndex = 4;
            this.LB_Char5.Text = "*";
            // 
            // LB_Char6
            // 
            this.LB_Char6.AutoSize = true;
            this.LB_Char6.Location = new System.Drawing.Point(548, 170);
            this.LB_Char6.Name = "LB_Char6";
            this.LB_Char6.Size = new System.Drawing.Size(13, 17);
            this.LB_Char6.TabIndex = 5;
            this.LB_Char6.Text = "*";
            // 
            // lbl7
            // 
            this.lbl7.AutoSize = true;
            this.lbl7.Location = new System.Drawing.Point(125, 289);
            this.lbl7.Name = "lbl7";
            this.lbl7.Size = new System.Drawing.Size(73, 17);
            this.lbl7.TabIndex = 6;
            this.lbl7.Text = "Lives Left:";
            // 
            // BTN_Guess
            // 
            this.BTN_Guess.Location = new System.Drawing.Point(519, 384);
            this.BTN_Guess.Name = "BTN_Guess";
            this.BTN_Guess.Size = new System.Drawing.Size(75, 23);
            this.BTN_Guess.TabIndex = 7;
            this.BTN_Guess.Text = "Guess";
            this.BTN_Guess.UseVisualStyleBackColor = true;
            this.BTN_Guess.Click += new System.EventHandler(this.BTN_Guess_Click);
            // 
            // BTN_Play
            // 
            this.BTN_Play.Location = new System.Drawing.Point(632, 384);
            this.BTN_Play.Name = "BTN_Play";
            this.BTN_Play.Size = new System.Drawing.Size(75, 23);
            this.BTN_Play.TabIndex = 8;
            this.BTN_Play.Text = "Play";
            this.BTN_Play.UseVisualStyleBackColor = true;
            this.BTN_Play.Click += new System.EventHandler(this.BTN_Play_Click);
            // 
            // textBoxGuess
            // 
            this.textBoxGuess.Location = new System.Drawing.Point(394, 385);
            this.textBoxGuess.Name = "textBoxGuess";
            this.textBoxGuess.Size = new System.Drawing.Size(100, 22);
            this.textBoxGuess.TabIndex = 9;
            this.textBoxGuess.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(594, 225);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(0, 17);
            this.linkLabel1.TabIndex = 10;
            // 
            // LB_LifeValue
            // 
            this.LB_LifeValue.AutoSize = true;
            this.LB_LifeValue.Location = new System.Drawing.Point(199, 289);
            this.LB_LifeValue.Name = "LB_LifeValue";
            this.LB_LifeValue.Size = new System.Drawing.Size(16, 17);
            this.LB_LifeValue.TabIndex = 11;
            this.LB_LifeValue.Text = "5";
            // 
            // TB_missedwords
            // 
            this.TB_missedwords.Location = new System.Drawing.Point(617, 28);
            this.TB_missedwords.Name = "TB_missedwords";
            this.TB_missedwords.Size = new System.Drawing.Size(157, 22);
            this.TB_missedwords.TabIndex = 12;
   
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TB_missedwords);
            this.Controls.Add(this.LB_LifeValue);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.textBoxGuess);
            this.Controls.Add(this.BTN_Play);
            this.Controls.Add(this.BTN_Guess);
            this.Controls.Add(this.lbl7);
            this.Controls.Add(this.LB_Char6);
            this.Controls.Add(this.LB_Char5);
            this.Controls.Add(this.LB_Char4);
            this.Controls.Add(this.LB_Char3);
            this.Controls.Add(this.LB_Char2);
            this.Controls.Add(this.LB_Char1);
            this.Name = "Form1";
            this.Text = "Hangman";
       
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LB_Char1;
        private System.Windows.Forms.Label LB_Char2;
        private System.Windows.Forms.Label LB_Char3;
        private System.Windows.Forms.Label LB_Char4;
        private System.Windows.Forms.Label LB_Char5;
        private System.Windows.Forms.Label LB_Char6;
        private System.Windows.Forms.Label lbl7;
        private System.Windows.Forms.Button BTN_Guess;
        private System.Windows.Forms.Button BTN_Play;
        private System.Windows.Forms.TextBox textBoxGuess;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label LB_LifeValue;
        private System.Windows.Forms.TextBox TB_missedwords;
    }
}

