﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hangman
{
    static class GetWord
    {
        public static string WordGetter()
        {
            string unsortedwords = "master,bother,bucket,legend,method,endure,middle,exceed,punish,monkey,course,borrow,strain,agency,answer,global,timber,return";
            List<string> sortedwords = unsortedwords.Split(',').ToList();
            int index = new Random().Next(sortedwords.Count);
            string secretword = sortedwords[index];
            return secretword;
        }
    }
}
